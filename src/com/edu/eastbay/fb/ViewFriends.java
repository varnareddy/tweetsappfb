package com.edu.eastbay.fb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.edu.eastbay.gau.GAEDataStore;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;

public class ViewFriends extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user_info = (User)session.getAttribute(ConstantNames.LOGIN_USER);
		String auth_token = (String) session.getAttribute(ConstantNames.O_AUTH_TOKEN);
		GAEDataStore gaeDataStoreInstance = GAEDataStore.getInstance();
		
		FacebookClient facebookClient = new DefaultFacebookClient(auth_token);
		Connection<User> myFriends = facebookClient.fetchConnection("/me/friends", User.class);
		
		PrintWriter writer = response.getWriter();
		writer.print("<table><tr><th>Photo</th><th>Name</th><th>Id</th></tr>");
		for (List<User> myFriendsList : myFriends) {
			writer.print("<tr>Entered" +myFriendsList.size()+"</tr>");
			for(User user: myFriendsList) {
				writer.print("<tr>Entered User</tr>");
				writer.print("<tr><td><img src=\"https://apps.facebook.com/" + user.getId() + "/picture\"/></td><td>" + user.getName() +"</td><td>" + user.getId() + "</td></tr>");
				//writer.print("<tr><td>First name: "+user.getName()+ "User id:"+user.getId()+"</td></tr>");
			}

		}
		writer.print("</table>"); 
		return;
		//writer.print("</table>");
		//writer.close();
		
	}
}
