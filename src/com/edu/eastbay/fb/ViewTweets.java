package com.edu.eastbay.fb;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.edu.eastbay.gau.GAEDataStore;
import com.restfb.types.User;

public class ViewTweets extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute(ConstantNames.LOGIN_USER);
		GAEDataStore gaeDataStoreInstance = GAEDataStore.getInstance();
		
		session.setAttribute(ConstantNames.USER_TWEETS_MAP, gaeDataStoreInstance.displayTweets(user.getId()));
		request.getRequestDispatcher("viewMyTweets.jsp").forward(request, response);
		//response.getWriter().println("<h1>View tweet Called</h1>");
		return;
	}
}
