package com.edu.eastbay.fb;

public class TweetData {
	String userID;
	String userName;
	String textContent;
	String tweetOption;
	Long tweetCount;
	String tweetDate;
	Long id;
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getTextContent() {
		return textContent;
	}
	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}
	public String getTweetOption() {
		return tweetOption;
	}
	public void setTweetOption(String tweetOption) {
		this.tweetOption = tweetOption;
	}
	public Long getTweetCount() {
		return tweetCount;
	}
	public void setTweetCount(Long tweetCount) {
		this.tweetCount = tweetCount;
	}
	public String getTweetDate() {
		return tweetDate;
	}
	public void setTweetDate(String tweetDate) {
		this.tweetDate = tweetDate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
}
