package com.edu.eastbay.gau;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.edu.eastbay.fb.ConstantNames;
import com.edu.eastbay.fb.TweetData;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.SortDirection;

public class GAEDataStore {
	private static GAEDataStore gaeDataStoreInstance = null;
	protected GAEDataStore(){
		
	}
	
	public static GAEDataStore getInstance() {
		if(gaeDataStoreInstance == null)
			gaeDataStoreInstance = new GAEDataStore(); 
		return gaeDataStoreInstance;
	}
	
	DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
	DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date();
	
	public Entity saveUserTweet(String userID, String userName, String textContent, String tweetOption) {
		Entity entity = new Entity(ConstantNames.USER_TWEETS);
		entity.setProperty(ConstantNames.LOGIN_USER_ID, userID);
		entity.setProperty(ConstantNames.LOGIN_USER_NAME, userName);
		entity.setProperty(ConstantNames.TWEET_CONTENT, textContent);
		entity.setProperty(ConstantNames.TWEET_OPTION, tweetOption);
		entity.setProperty(ConstantNames.COUNT, 0);
		date = new Date();
		entity.setProperty(ConstantNames.DATE, sdf.format(date));
		Long id = saveEntity(entity).getId();
		Key key = KeyFactory.createKey(ConstantNames.USER_TWEETS, id);
		Entity ent = null;
		try {
			 ent = dataStore.get(key);
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ent;
	}
	
	public Map<Long, TweetData> displayTweets(String userID) {
		Entity entity = new Entity(ConstantNames.USER_TWEETS);
		Query query = new Query(ConstantNames.USER_TWEETS);
		PreparedQuery pq= dataStore.prepare(query);
		TweetData tweetData;
		Map<Long, TweetData> userTweets = new HashMap<Long, TweetData>();
		for(Entity output: pq.asIterable()) {
			if(output.getProperty(ConstantNames.LOGIN_USER_ID) != null && output.getProperty(ConstantNames.LOGIN_USER_ID).equals(userID)) {
				tweetData = new TweetData();
				tweetData.setId(output.getKey().getId());
				tweetData.setUserID((String) output.getProperty(ConstantNames.LOGIN_USER_ID));
				tweetData.setUserName((String) output.getProperty(ConstantNames.LOGIN_USER_NAME));
				tweetData.setTextContent((String) output.getProperty(ConstantNames.TWEET_CONTENT));
				tweetData.setTweetCount((Long) output.getProperty(ConstantNames.COUNT));
				tweetData.setTweetDate((String) output.getProperty(ConstantNames.DATE));
				
				userTweets.put(tweetData.getId(), tweetData);
				
				output.setProperty(ConstantNames.COUNT, (Long) output.getProperty(ConstantNames.COUNT) + 1);
				saveEntity(output);
			}
		}
		
		return userTweets;
	}
	
	public List<TweetData> topTenTweets(){
		Entity entity = new Entity(ConstantNames.USER_TWEETS);
		Query query = new Query(ConstantNames.USER_TWEETS).addSort(ConstantNames.COUNT, SortDirection.DESCENDING);
		PreparedQuery pq= dataStore.prepare(query);
		List<TweetData> topTweets = new LinkedList<TweetData>();
		TweetData tweetData;
		int entires=0;
		for(Entity output: pq.asIterable()) {
			if(entires>10) {
				return topTweets;
			}
			tweetData = new TweetData();
			
			tweetData.setId(output.getKey().getId());
			tweetData.setUserID((String) output.getProperty(ConstantNames.LOGIN_USER_ID));
			tweetData.setUserName((String) output.getProperty(ConstantNames.LOGIN_USER_NAME));
			tweetData.setTextContent((String) output.getProperty(ConstantNames.TWEET_CONTENT));
			tweetData.setTweetCount((Long) output.getProperty(ConstantNames.COUNT));
			tweetData.setTweetDate((String) output.getProperty(ConstantNames.DATE));
			
			topTweets.add(tweetData);
			
			output.setProperty(ConstantNames.COUNT, (Long) output.getProperty(ConstantNames.COUNT) + 1);
			saveEntity(output);
			
			entires++;
		}
		return topTweets;
	}
	
	public void removeUserTweet(Long tweetID) {
		try {
			Entity entity = dataStore.get(KeyFactory.createKey(ConstantNames.USER_TWEETS, tweetID));
			dataStore.delete(entity.getKey());
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Key saveEntity(Entity entity){
		return dataStore.put(entity);
	}
	
	
	private void removeEntity(Entity entity){
		//dataStore.de
	}
}
