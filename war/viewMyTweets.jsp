<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.edu.eastbay.fb.*, com.restfb.types.User, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>View My tweets</title>
</head>
<body>
	<jsp:include page="NavBar.jsp"/>
	<%
		Map<Long, TweetData> userTweets = (Map<Long, TweetData>)request.getSession().getAttribute(ConstantNames.USER_TWEETS_MAP);
	%>
	<form action="/deleteUserTweet" method="post">
		<table border="1">
	<tr>
		<td>Name</td>
		<td>Tweet Content</td>
		<td>Date</td>
		<td>View count</td>
		<td>Delete</td>
	</tr>
	<%
		TweetData tweetData = null;
		for(Map.Entry<Long, TweetData> data : userTweets.entrySet()) {
			tweetData = data.getValue();			
	%>
	<tr>
		<td><%=tweetData.getUserName() %></td>
		<td><%=tweetData.getTextContent() %></td>
		<td><%=tweetData.getTweetDate() %></td>
		<td><%=tweetData.getTweetCount() %></td>
		<td><input type="submit" name="Delete" id="Delete" style="color: white; background-color: white;"" value="<%=tweetData.getId()%>"/></td>
	</tr>
	<% }//End For Each %>
	</table>
	</form>
</body>
</html>