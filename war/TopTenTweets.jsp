<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.*, com.edu.eastbay.gau.*, com.restfb.*, com.edu.eastbay.fb.*, com.restfb.types.User, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>TopTenTweets</title>
</head>
<body>
	<jsp:include page="NavBar.jsp"/>

<table border="1">
<tr>
<th>Image</th>
<th>User</th>
<th>Tweet</th>
<th>Date</th>
<th>Count</th>
</tr>
<%
User user_info = (User)session.getAttribute(ConstantNames.LOGIN_USER);
String auth_token = (String) session.getAttribute(ConstantNames.O_AUTH_TOKEN);
GAEDataStore gaeDataStoreInstance = GAEDataStore.getInstance();

List<TweetData> topTweets = gaeDataStoreInstance.topTenTweets();

TweetData tweetData = null;
for(TweetData data : topTweets) {
	String uID = data.getUserID();
%>
	<tr>
		<td><img src=<%= "\""+"https://graph.facebook.com/"+uID+ "/picture"+"\"" %> ></td>
		<td><%=data.getUserName() %></td>
		<td><%=data.getTextContent() %></td>
		<td><%=data.getTweetDate() %></td>
		<td><%=data.getTweetCount() %></td>
	</tr>	
<% }//End For Each %>

</table>

</body>
</html>
