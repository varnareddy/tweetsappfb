<?xml version="1.0" encoding="UTF-8" ?>
<%@page import="com.google.appengine.repackaged.com.google.api.client.http.HttpRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.restfb.types.User.*, java.io.*, com.edu.eastbay.gau.*, com.restfb.*, com.edu.eastbay.fb.*, com.restfb.types.User, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Insert title here</title>
</head>
<body>
	
	<jsp:include page="NavBar.jsp"/>
	
<table border="1"><tr><th>Photo</th><th>Name</th><th>Tweets</th></tr>
<%
User user_info = (User)session.getAttribute(ConstantNames.LOGIN_USER);
String auth_token = (String) session.getAttribute(ConstantNames.O_AUTH_TOKEN);
GAEDataStore gaeDataStoreInstance = GAEDataStore.getInstance();

FacebookClient facebookClient = new DefaultFacebookClient(auth_token);
Connection<User> myFriends = facebookClient.fetchConnection("/me/friends", User.class);


FriendDetails fDetails;
int numberOfFriends = 0;
String image = "";
for (List<User> myFriendsList : myFriends) {
	numberOfFriends = myFriendsList.size();
	image = "";
	for(User user: myFriendsList) {
		fDetails = new FriendDetails();
		fDetails.setImageSource("<img src=\"https://graph.facebook.com/"+user.getId() + "/picture\">");
		fDetails.setName(user.getName());
		fDetails.setUserID(user.getId());
%>
<tr><td><%=fDetails.getImageSource() %></td>
<td><%=fDetails.getName() %></td>
<%	Map<Long, TweetData> userTweets = gaeDataStoreInstance.displayTweets(fDetails.getUserID());
%> 
<td>
	<%
		TweetData tweetData = null;
		for(Map.Entry<Long, TweetData> data : userTweets.entrySet()) {
			tweetData = data.getValue();			
	%>
		<%=tweetData.getTextContent() +"      Date: " %> <%=tweetData.getTweetDate() + "       Count:" %> <%=tweetData.getTweetCount() %>
		<br/>

	<% }//End For Each %>
	</td></tr>
<%
	}
	
}

%>

</table>
</body>
</html>